var Promise = require("bluebird");
var db = Promise.promisifyAll(db);

function getUser(userId) {
  return Promise.try(function(){
    return db.findAsync({"selector": {"documentType": "user", "_id": userId}});
  }).then(function(userInfo){
    if (userInfo.docs.length > 1) {
      throw new Error("More than one user returned.");
    } else {
      return userInfo;
    }
  })
}

function saveMessage(userId, messageText) {
  return Promise.try(function(){
    return getUser(userId);
  }).then(function(userInfo){
    return db.insertAsync({type: 'message', message: messageText, userId: userInfo});
  }).then(function(result){
    /* Insertion successfully completed. */
  }).catch(function(err){
    /* Somewhere, an error occurred. You probably shouldn't handle it here, though, but in whatever is calling this function. */
  });
}